import requests
from getpass import getpass

# Base URL of the shared metrics service
BASE_URL = 'http://localhost:8001'

def get_access_token():
    # Prompt user for credentials
    username = input("Enter your username: ")
    password = getpass("Enter your password: ")

    # Send a POST request to the login endpoint to get an access token
    login_url = f"{BASE_URL}/login"
    response = requests.post(login_url, json={"username": username, "password": password})

    if response.status_code == 200:
        access_token = response.json().get('access_token')
        return access_token
    else:
        print("Invalid credentials. Please try again.")
        return None

def get_shared_metrics(access_token):
    # Send a GET request to the shared_metrics endpoint with the access token in the Authorization header
    shared_metrics_url = f"{BASE_URL}/shared_metrics"
    headers = {"Authorization": f"Bearer {access_token}"}

    response = requests.get(shared_metrics_url, headers=headers)

    if response.status_code == 200:
        shared_metrics = response.json().get('shared_metrics', [])
        return shared_metrics
    else:
        print(f"Failed to retrieve shared metrics: {response.json().get('error')}")

def main():
    access_token = get_access_token()

    if access_token:
        shared_metrics = get_shared_metrics(access_token)
        if shared_metrics:
            print("Shared Metrics:")
            for metric in shared_metrics:
                print(f"Timestamp: {metric['timestamp']}, Metric Name: {metric['metric_name']}, Metric Value: {metric['metric_value']}")
        else:
            print("No shared metrics found.")

if __name__ == '__main__':
    main()

