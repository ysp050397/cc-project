import sqlite3
from flask import Flask, request, jsonify
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity
from datetime import datetime

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'super-secret'  # Change this to a secure key in production
jwt = JWTManager(app)

# Hardcoded users for demonstration purposes
users = {
    "user1": "password1",
    "user2": "password2"
}

@app.route('/login', methods=['POST'])
def login():
    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username or not password:
        return jsonify({"message": "Missing username or password"}), 400

    if username not in users or users[username] != password:
        return jsonify({"message": "Invalid username or password"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token), 200

# SQLite database setup
DB_NAME = "data/health_metrics.db"


def init_db():
    conn = sqlite3.connect(DB_NAME)


# API endpoint to share a metric with another user
@app.route('/share_metric', methods=['POST'])
@jwt_required()
def share_metric():
    user_id = request.authorization.username
    data = request.json
    metric_id = data.get('metric_id')
    shared_with_user = data.get('shared_with_user')

    try:
        conn = sqlite3.connect(DB_NAME)
        c = conn.cursor()
        c.execute('''UPDATE health_metrics
                     SET shared_with = ?
                     WHERE id = ? AND user_id = ?''', (shared_with_user, metric_id, user_id))
        conn.commit()
        conn.close()
        return jsonify({"message": "Metric shared successfully"}), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500



def get_shared_metrics(user_id):
    try:
        conn = sqlite3.connect(DB_NAME)
        c = conn.cursor()
        c.execute('''SELECT hm.timestamp, hm.metric_name, hm.metric_value 
                     FROM health_metrics hm 
                     WHERE hm.shared_with = ?''', (user_id,))
        rows = c.fetchall()
        conn.close()

        metrics = [{"timestamp": row[0], "metric_name": row[1], "metric_value": row[2]} for row in rows]
        return metrics
    except Exception as e:
        return jsonify({"error": str(e)}), 500


@app.route('/shared_metrics', methods=['GET'])
@jwt_required()
def shared_metrics():
    user_id = get_jwt_identity()

    shared_metrics = get_shared_metrics(user_id)
    return jsonify({"shared_metrics": shared_metrics}), 200


if __name__ == '__main__':
    init_db()
    app.run(debug=True, host='0.0.0.0', port=8001)

