import requests
from requests.auth import HTTPBasicAuth

def login(username, password):
    url = f'http://localhost:8001/login'
    response = requests.post(url, json={'username': username, 'password': password})
    if response.status_code == 200:
        print("Login succeed")
        return response.json()['access_token']
    else:
        print(f"Login failed: {response.json()['message']}")
        return None

# Credentials for API authentication
username = "user1"
password = "password1"

# API endpoint URL
api_url = "http://localhost:8001/share_metric"

# Metric data to share
data = {
    "metric_id": 1,  # Example metric ID to share
    "shared_with_user": "user2"  # User to share the metric with
}

# Make HTTP POST request to share metric
try:
    # Login to obtain access token
    access_token = login(username, password)
    headers = {'Authorization': f'Bearer {access_token}','Content-Type': 'application/json'}
    response = requests.post(api_url,json=data, headers=headers)
    if response.status_code == 200:
        print("Retrieved metrics successfully:")
        print(response.json())
    else:
        print(f"Failed to share metric. Error: {response.text}")
except requests.exceptions.RequestException as e:
    print(f"Error occurred: {e}")
