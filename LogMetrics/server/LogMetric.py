import sqlite3
from flask import Flask, request, jsonify
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity
from datetime import datetime

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'super-secret'  # Change this to a secure key in production
jwt = JWTManager(app)

# Hardcoded users for demonstration purposes
users = {
    "user1": "password1",
    "user2": "password2"
}

@app.route('/login', methods=['POST'])
def login():
    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username or not password:
        return jsonify({"message": "Missing username or password"}), 400

    if username not in users or users[username] != password:
        return jsonify({"message": "Invalid username or password"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token), 200

# SQLite database setup
DB_NAME = "data/health_metrics.db"


def init_db():
    conn = sqlite3.connect(DB_NAME)
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS health_metrics
                 (id INTEGER PRIMARY KEY AUTOINCREMENT,
                 user_id INTEGER,
                 timestamp TEXT,
                 metric_name TEXT,
                 metric_value REAL,
                 shared_with INTEGER DEFAULT NULL)''')
    conn.commit()
    conn.close()



# Health metrics logging endpoint
@app.route('/log_metrics', methods=['POST'])
@jwt_required()
def log_metrics():
    user_id = get_jwt_identity()
    data = request.json
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print(data)
    try:
        conn = sqlite3.connect(DB_NAME)
        c = conn.cursor()
        for metric_name, metric_value in data.items():
            c.execute('''INSERT INTO health_metrics (user_id, timestamp, metric_name, metric_value)
                         VALUES (?, ?, ?, ?)''', (user_id, timestamp, metric_name, metric_value))
        conn.commit()
        conn.close()
        return jsonify({"message": "Metrics logged successfully"}), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500


# API endpoint to retrieve health metrics
@app.route('/get_metrics', methods=['GET'])
@jwt_required()
def get_metrics():
    user_id = get_jwt_identity()

    try:
        conn = sqlite3.connect(DB_NAME)
        c = conn.cursor()
        c.execute('''SELECT timestamp, metric_name, metric_value FROM health_metrics
                     WHERE user_id = ?''', (user_id,))
        rows = c.fetchall()
        conn.close()

        metrics = [{"timestamp": row[0], "metric_name": row[1], "metric_value": row[2]} for row in rows]
        return jsonify({"metrics": metrics}), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500


if __name__ == '__main__':
    init_db()
    app.run(debug=True,host='0.0.0.0',port=8000)
