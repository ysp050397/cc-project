#################################################################
Docker build 
#################################################################

Build Docker: docker build -t log-metrics .



#################################################################
Docker start 
#################################################################

Run Docker: docker run -d -p 8000:8000 -v ./server:/app -v <pathtodatabase>:/app/data --name log_metrics_container log-metrics
