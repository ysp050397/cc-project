import requests

BASE_URL = 'http://127.0.0.1:8000'

def login(username, password):
    url = f'{BASE_URL}/login'
    response = requests.post(url, json={'username': username, 'password': password})
    if response.status_code == 200:
        print("Login succeed")
        return response.json()['access_token']
    else:
        print(f"Login failed: {response.json()['message']}")
        return None

def log_metrics(access_token, metrics):
    url = f'{BASE_URL}/log_metrics'
    headers = {'Authorization': f'Bearer {access_token}'}
    response = requests.post(url, headers=headers, json=metrics)
    if response.status_code == 200:
        print("Metrics logged successfully")
    else:
        print(f"Failed to log metrics: {response.json()['message']}")

def get_metrics(access_token):
    url = f'{BASE_URL}/get_metrics'
    headers = {'Authorization': f'Bearer {access_token}'}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        print("Retrieved metrics successfully:")
        print(response.json())
    else:
        print(f"Failed to retrieve metrics: {response.json()['message']}")

if __name__ == '__main__':
    # Example usage
    username = 'user1'
    password = 'password1'

    # Login to obtain access token
    access_token = login(username, password)
    if access_token:
        # Log health metrics
        metrics_to_log = {
            "blood_pressure": "120/80",
            "glucose_level": 100,
            "weight": 70,
            "height": 182
        }
        log_metrics(access_token, metrics_to_log)

        # Retrieve health metrics
        get_metrics(access_token)
